## 1.9 Texteingabe zählen

**Voraussetzung**: Vorlesung zum Thema JavaFX / FXML

**Ziel**: Erstellen einer einfachen JavaFX-Anwendung nach dem MVVM-Muster ohne Persistenz.  

**Dauer**: 2h 

Programmieren Sie eine JavaFXML-Anwendung, die einem Nutzer die Möglichkeit bietet, einen Text in einem Textfeld einzugeben. Sobald der Nutzer auf die Schaltfläche "Save" klickt, merkt sich die Anwendung den Text im Speicher und leert das Textfeld wieder. In der Oberfläche sollen zwei Informationen sichtbar sein. Zum einen sollen alle getätigten Texteingaben angezeigt werden. Dabei soll in der UI eine Texteingabe nur einmal angezeigt werden, auch wenn die Eingabe mehrfach gemacht wurde. Zusätzlich soll angezeigt werden, wie oft die Texteingabe gemacht wurde. Darüber hinaus soll die Gesamtzahl der Eingaben in der UI angezeigt werden. Letztendlich soll die UI auch eine Schaltfläche "Clear" zum Löschen aller Eingaben besitzen.

**(a) Programmieren Sie die Klasse `TextInputFrequencyStore`**, die das Model der Anwendung im Sinne des MVVM-Musters repräsentiert. Verwenden Sie zum Speichern der Texteingaben eine `TreeMap`, die als Schlüssel die Texteingabe und als Wert die Häufigkeit der Texteingabe verwaltet. Die Klasse soll die folgenden Methoden besitzen:

- `public void addTextInput(String textInput)` - Fügt eine Texteingabe in die Map hinzu und aktualisiert die Häufigkeit des Eintrags in der Map.
- `public int getTotalNumberOfTextInputs()` - Ermittelt die Gesamtzahl der Texteingaben und gibt diese als Ergebnis der Methode zurück.
- `public void clear()` - Leert die Map.
- `public String toString()` - Gibt die Map als String zurück. Verwenden Sie dabei die existierende `toString()`-Methode der Map.

**(b) Programmieren Sie die Klasse `TextInputFrequencyStoreUI`**, die das FXML-Setup der Anwendung beinhaltet. müssen 
Stellen Sie sicher, dass alle oben beschriebenen Elemente Teil der UI sind. 
Benutzen Sie zu erstellen des UI den Scene Builder und speichern Sie die entsprechende `FXML`-Datei im Ordner `resources/fxml`

**Hinweis**: Achten Sie auch darauf, dass die Texteingabe validiert wird, sodass beispielsweise bei der Eingabe eines leeren Texts der Benutzer eine entsprechende Meldung erhält.